Algoritmo seleccionmultiple
	nota <- 0
	salida <- 'a'
	Leer nota
	Si nota>5 Entonces
		Si nota>6 Entonces
			Si nota>7 Entonces
				Si nota>9 Entonces
					salida <- 'sobresaliente'
				SiNo
					salida <- 'notable'
				FinSi
			SiNo
				salida <- 'bien'
			FinSi
		SiNo
			salida <- 'suficiente'
		FinSi
	SiNo
		salida <- 'suspenso'
	FinSi
	Escribir salida
FinAlgoritmo

